**Postgres Kafka Pipeline**

Quick POC demo of moving data into a kafka topic for data transformations via spark pipelines and streamsets datacollector to monitor performance metrics between the two


---

## Setting up Kafka Build

Spotify/Kafka Docker Image is used for the implementation of POC.
To install do the following:

1. Visit the link: https://hub.docker.com/r/spotify/kafka/
2. Follow installation steps for performing docker pull as well as setup
3. Run from terminal:  $ sudo docker exec -it <container_name> bash
4. From the box run:  /opt/kafka-<version_here>bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic test
5. Exit container ctrl + D

---

## Setting up Gradle Environment
